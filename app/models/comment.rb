class Comment < ActiveRecord::Base
	belongs_to :user
	validates_length_of :body, :in => 20..400,:message =>"longitud no valida"
end
